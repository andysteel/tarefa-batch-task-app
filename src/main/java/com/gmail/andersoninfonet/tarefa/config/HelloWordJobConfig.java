package com.gmail.andersoninfonet.tarefa.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.gmail.andersoninfonet.tarefa.task.HelloWordTask;

@Configuration
@EnableBatchProcessing
@Import(DataConfig.class)
public class HelloWordJobConfig {

	@Autowired	
	private JobBuilderFactory jobBuilders;

	@Autowired	
	private StepBuilderFactory stepBuilders;

	@Bean
	public Job helloWorldJob() {
		return jobBuilders.get("helloWorldJob")
				.incrementer(new RunIdIncrementer())
				.preventRestart()
				.start(step()).build();
	}

	@Bean
	public Step step() {
		return stepBuilders.get("step")
				.tasklet(tasklet())
				.build();
	}

	@Bean
	public Tasklet tasklet() {
		return new HelloWordTask();
	}
	
}
