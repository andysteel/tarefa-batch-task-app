package com.gmail.andersoninfonet.tarefa.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DataConfig {

	@Bean
	public DataSource dataSource() {
		final HikariDataSource ds = new HikariDataSource();
		try {
			ds.setDriverClassName("org.postgresql.Driver");
			ds.setJdbcUrl("jdbc:postgresql://localhost:5432/tarefa");
			ds.setUsername("root");
			ds.setPassword("502010");
			ds.setMinimumIdle(3);
			ds.setMaximumPoolSize(10);
			ds.setConnectionTestQuery("SELECT 1");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ds;
		
//		final HikariDataSource ds = new HikariDataSource();
//		try {
//			ds.setDriverClassName("org.h2.Driver");
//			ds.setJdbcUrl("jdbc:h2:tcp://localhost:19092/mem:dataflow");
//			ds.setUsername("sa");
//			ds.setPassword("");
//			ds.setMinimumIdle(3);
//			ds.setMaximumPoolSize(10);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return ds;
	}
}
